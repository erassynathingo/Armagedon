/**
 * @description The simplified HTTP request client 'request' with Promise support.
 */
const request = require('request-promise');


module.exports = {
    /**
   * @description sends https request with given options as parameters
   * @name send
   * {@link https://www.npmjs.com/package/request-promise}
   * @param {Object} options - http request options
   * @returns {Promise}  resolves | reject Promise body or error
   */
    send: options => request(options)
}