/**
 * logger.lib.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 * @description Logs to the console && file logs
 */

const _config = require(`../config/api.config`);
const bunyan = require('bunyan');
const fs = require('fs');

const logPath = './logs/' + _config.title + '.error.log';
const info_logPath = './logs/' + _config.title + '.trace.log';
const fatalLogPath = './logs/' + _config.title + '.fatal_logs.log';

let logger = bunyan.createLogger(
  {
    name: _config.title,
    streams: [
      {
        level: 'trace',
        name: 'Trace_Log',
        path: info_logPath, // log info and above to a file
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 1, // keep 2 back copies2
        name: 'Trace Logs'
      },
      {
        level: 'error',
        path: logPath, // log ERROR and above to a file
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 1, // keep 2 back copies
        name: 'Errors'
      },
      {
        level: 'fatal',
        path: fatalLogPath, // log ERROR and above to a file
        type: 'rotating-file',
        period: '1d', // daily rotation
        count: 1, // keep 2 back copies
        name: 'Fatal_Errors'
      }
    ]
  });

const LOG = function () {
  this.log = item => {
    console.log(item);
    _config.logs.file == true?  logger.trace(item) : null
    return this;
  };
  this.trace = item => {
    console.log(item);
    _config.logs.file == true?  logger.trace(item) : null
    return this;
  };
  this.error = text => {
    console.error(item);
    _config.logs.file == true?  logger.error(item) : null
    return this;
  };

  this.debug = text => {
    console.debug(item);
    _config.logs.file == true?  logger.debug(item) : null
    return this;
  };

  this.warn = text => {
    console.warn(item);
    _config.logs.file == true?  logger.warn(item) : null
    return this;
  };

  this.info = text => {
    console.info(item);
    _config.logs.file == true?  logger.trace(item) : null
    return this;
  };
};

module.exports = new LOG();
