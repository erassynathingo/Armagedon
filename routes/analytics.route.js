/**
 * analytics.route.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */


const share = require(`../config/share.config`);
const Analytics = require('../core/analytics.ctrl');
const analytics = new Analytics();

let routes = [
    {
        method: 'POST',
        path: '/analytics',
        handler: async () => {
            return 'controller functions'
        }
    },
    {
        method: 'PATCH',
        path: '/analytics',
        options: {
        },
        handler: async () => {
            return 'controller functions'
        }
    },
    {
        method: 'GET',
        path: '/analytics/findStrategy',
        options: {
            pre: [
                {
                    method: (request, h) => {
                        return 'Hello';
                    }, assign: 'm1'
                }
            ]
        },
        handler: async (request, h) => {
            analytics.findStrategy(share.tests.findStrategy.candlesticks, share.tests.findStrategy.options)
            .catch(error=> console.log("ERRRR: ",error));

            return "Hala";
        }
    },
    {
        method: 'DELETE',
        path: '/analytics/{id?}',
        options: {
        },
        handler: async () => {
            return 'controller functions'
        }
    },
    {
        method: 'UPDATE',
        path: '/analytics/{id?}',
        handler: async () => {
            return 'controller functions'
        }
    }
]

module.exports = routes;