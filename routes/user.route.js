/**
 * user.route.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */

const share = require(`../config/share.config`);

const userRoutes = [
    {
        method: 'POST',
        path: '/users',
        handler: async () => {
            return 'controller functions'
        }
    },
    {
        method: 'PATCH',
        path: '/users',
        options: {
        },
        handler: async () => {
            return 'controller functions'
        }
    },
    {
        method: 'GET',
        path: '/users/{id?}',
        options: {
            pre: [
                {
                    method: (request, h) => {
                        console.log('This one first');
                        return 'request.pre.m1';
                    }, assign: 'm1'
                }
            ]
        },
        handler: async (request, h) => {
            share.logger.log("Route Accessed 2nd");
            return request.pre.m1 + '!\n';
        }
    },
    {
        method: 'DELETE',
        path: '/users/{id?}',
        options: {
        },
        handler: async () => {
            return 'controller functions'
        }
    },
    {
        method: 'UPDATE',
        path: '/users/{id?}',
        handler: async () => {
            return 'controller functions'
        }
    }
]

module.exports = userRoutes;