/**
 * analytics.ctrl.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */
const share = require(`../config/share.config`);

const analytics = require('forex.analytics');

class Analytics {

    constructor(){
        

    }

    /**
     * @name findStrategy
     * @description Finds the optimal strategy for a certain period defined by the candlesticks array.
     * @param candlesticks parameter should contain an array of objects representing one candlestick in OHLC chart.
     * @param options parameter lists several properties that influence the genetic algorithm
     * {@link https://www.npmjs.com/package/forex.analytics#findstrategycandlesticks-options-progresscallback}
     * 
     * @returns {Promise<Object>} {strategy, fitness, generation}
     */

    async findStrategy(candlesticks, options){
        try{
            analytics.findStrategy(candlesticks, options, (strategy, fitness, generation)=>{
                console.log('Fitness: ' + fitness + '; Generation: ' + generation);
            }).then(strategy=>console.log("Strategy: ", strategy)).catch(error=>console.log("E: ", error));
        }catch(error){
            console.log("Err: ", error)
            return error
        }
        
    }

    /**
     * @name getMarketStatus
     * @description Returns suggestion whether to buy or sell current for the last candlestick in the candlesticks array passed in as a first parameter.
     * @param candlesticks parameter should contain an array of objects representing one candlestick in OHLC chart.
     * @param options  parameter lists one property
     * {@link https://www.npmjs.com/package/forex.analytics#getmarketstatuscandlesticks-options}
     * 
     * @returns {Promise<Object>} { shouldBuy: true, shouldSell: false }
     */
    async getMarketStatus(candlesticks, options){

    }

    /**
     * @name getTrades
     * @description Returns an array of trades that were performed on a provided candlestick array with given strategy candlesticks array passed in as a first parameter.
     * @param candlesticks parameter should contain an array of objects representing one candlestick in OHLC chart.
     * @param {Object} options  parameter lists one property
     * {@link https://www.npmjs.com/package/forex.analytics#gettradescandlesticks-options}
     * 
     * @returns {Promise<Object>} { shouldBuy: true, shouldSell: false, sureness: 95% }
     */
    async getTrades(candlesticks, options){

    }
}

module.exports = Analytics;