/**
 * exante.api.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */

const share = require('../config/share.config');

const demo_configs = {
    headers: {
        'Authorization': 'Basic ZWJmMjgxNTItMTJiMC00NzJmLThkYTgtY2ZjOTgzZDhmYTBhOndYdDhQWktxaHR4amtSSTc3Y093',
        'Cache-control': 'no-cache'
      },
    url: 'https://api-demo.exante.eu/md/1.0'
}
/**
 * @description Market Data API is a mixed HTTP REST and HTTP streaming API. It provides access to historical and live quotes data on a wide range of symbols.
 */
class Exante {

  constructor (authOptions) {}

  /**
   * @name fetchOHLC
   * @description OHLC stands for Open-High-Low-Close — candles — and can be used to build candlestick charts. Several aggregation intervals are available.
   * @param 
   * @returns {Array<Object>} e.g. [{ "timestamp": 1481572800000, "open": 112.975, "high": 113.105, "low": 112.935, "close": 112.965}]
   */

  async fetchOHLC(){
      demo_configs.url = await this.buildUrl()
      return await share.request()
  }

  async buildUrl(){

  }

  /**
   * 
   * @param {Object} queries 
   */
  buildQueryParams(queries){

  }
}

module.exports = new Exante(base_configs);
