/**
 * index.js
 * @description entry point for Armagedon API
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018 Techsurge Investments cc
 * All rights reserved
 */

'use strict';

const routes = require(`./config/routes.config`);
const shared = require(`./config/share.config`);



const server = shared.Hapi.server({
    port: shared._config.port,
    host: shared._config.hostname
});

const init = async () => {
    await server.start();
    await server.route(await routes);
    console.log(`Running ${shared._config.title}: ${server.info.uri}`);

};
process.on('unhandledRejection', (err) => {

    console.log('Error: ',err);
    process.exit(1);
});
init();