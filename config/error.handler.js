/**
 * error-handler.js
 *
 * @author Erastus Nathingo <contacterassy.com>
 * @copyright (c) 2017 Logic Plus Information Technologies CC
 * @license MIT
 */
const errorHandler = require('../lib/api.error-handler');
module.exports = errorHandler;