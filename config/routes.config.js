/**
 * routes.config.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */

const share = require(`./share.config`);
const usersRoutes = require(`../routes/user.route`);
const analyticsRoutes = require(`../routes/analytics.route`);


const defaultRoute = [
    {
        method: 'GET',
        path: '/',
        handler: function (request, h) {
    
            return {
                API: share._config.title,
                '@Copyright': `${share._config.author} ${share._config.year}`,
                Version: '1.0.0'
              }
        }
    }
]

/** Contains all route arrays */
let routesCollection = [defaultRoute, usersRoutes, analyticsRoutes];

share.logger.log(`Adding routes to API..`.blue);
const routes = async () => [].concat.apply([], routesCollection);
module.exports = routes();
