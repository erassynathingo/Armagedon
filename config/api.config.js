const os = require('os');
const colors = require('colors');

const env = process.env.NODE_ENV || 'dev';
const paths = {}

const dev = {
    author: `Erastus Nathingo </ contact@erassy.com >`,
    year: 2018,
    port: 3000,
    hostname: 'localhost',
    title: 'Armagedon {dev}',
    db_url: '',
    jwtOptions: {
        secret: '',
        cookie: ''
    },
    paths: paths,
    logs: {
        trace: false,
        debug: false,
        console: true,
        file: false
    },
    db: {
        url: '',
        user: '',
        pass: ''
    }
}

const prod = {
    author: `Erastus Nathingo </ contact@erassy.com >`,
    year: 2018,
    port: 3000,
    hostname: os.hostname(),
    title: 'Armagedon {Prod}',
    db_url: '',
    jwtOptions: {
        secret: '',
        cookie: ''
    },
    paths: paths,
    logs: {
        trace: false,
        debug: false,
        console: false,
        file: false
    },
    db: {
        url: '',
        user: '',
        pass: ''
    }
}


const config = {dev,prod};
module.exports = config[env];