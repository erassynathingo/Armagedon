/**
 * share.config.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 * @description shares all the requirements needed for the API
 */
const Hapi = require('hapi');
const _config = require(`./api.config`);
const logger = require(`../lib/logger.lib`);
const routes = require(`./routes.config`);
const request = require('../lib/http.request.lib');
const tests = require('../tests/analytics.data');
const AnalyticsClass = require('../core/analytics.ctrl');

const shared = {
  /** Logging events {log, debug, warn, error} */
  logger: {
    log: (text) => _config.logs.console == true ? logger.log(text) : null,
    debug: (text) => _config.logs.console == true ? logger.debug(text) : null,
    warn: (text) => _config.logs.console == true ? logger.warn(text) : null,
    error: (text) => _config.logs.console == true ? logger.error(text) : null
  },

  _config: _config,
  Hapi: Hapi,
  routes: routes,
  request: request,
  tests: tests,
  AnalyticsClass: AnalyticsClass
};
module.exports = shared;
