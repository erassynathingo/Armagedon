let globalCandleSticks = [
    {
        "timestamp": 1527077700000,
        "open": 1.290785,
        "low": 1.29022,
        "close": 1.29024,
        "high": 1.290795
    },
    {
        "timestamp": 1527077400000,
        "open": 1.29138,
        "low": 1.290625,
        "close": 1.29079,
        "high": 1.29157
    },
    {
        "timestamp": 1527077100000,
        "open": 1.290895,
        "low": 1.29089,
        "close": 1.29136,
        "high": 1.29157
    },
    {
        "timestamp": 1527076800000,
        "open": 1.29038,
        "low": 1.29037,
        "close": 1.290875,
        "high": 1.29149
    },
    {
        "timestamp": 1527076500000,
        "open": 1.29045,
        "low": 1.290285,
        "close": 1.290375,
        "high": 1.290775
    },
    {
        "timestamp": 1527076200000,
        "open": 1.29049,
        "low": 1.290365,
        "close": 1.290455,
        "high": 1.29075
    },
    {
        "timestamp": 1527075900000,
        "open": 1.290265,
        "low": 1.289995,
        "close": 1.290495,
        "high": 1.2906
    },
    {
        "timestamp": 1527075600000,
        "open": 1.290115,
        "low": 1.290115,
        "close": 1.29026,
        "high": 1.29059
    },
    {
        "timestamp": 1527075300000,
        "open": 1.290315,
        "low": 1.289795,
        "close": 1.29012,
        "high": 1.29047
    },
    {
        "timestamp": 1527075000000,
        "open": 1.29019,
        "low": 1.289785,
        "close": 1.290335,
        "high": 1.290395
    },
    {
        "timestamp": 1527074700000,
        "open": 1.290135,
        "low": 1.290035,
        "close": 1.290185,
        "high": 1.290605
    },
    {
        "timestamp": 1527074400000,
        "open": 1.29081,
        "low": 1.289865,
        "close": 1.29014,
        "high": 1.29096
    },
    {
        "timestamp": 1527074100000,
        "open": 1.29034,
        "low": 1.29034,
        "close": 1.290805,
        "high": 1.29088
    },
    {
        "timestamp": 1527073800000,
        "open": 1.2899,
        "low": 1.289805,
        "close": 1.290315,
        "high": 1.290385
    },
    {
        "timestamp": 1527073500000,
        "open": 1.289225,
        "low": 1.289225,
        "close": 1.28989,
        "high": 1.289905
    },
    {
        "timestamp": 1527073200000,
        "open": 1.2894,
        "low": 1.289225,
        "close": 1.28923,
        "high": 1.2899
    },
    {
        "timestamp": 1527072900000,
        "open": 1.28922,
        "low": 1.28914,
        "close": 1.28939,
        "high": 1.289505
    },
    {
        "timestamp": 1527072600000,
        "open": 1.28911,
        "low": 1.288875,
        "close": 1.289215,
        "high": 1.28931
    },
    {
        "timestamp": 1527072300000,
        "open": 1.288985,
        "low": 1.28898,
        "close": 1.289105,
        "high": 1.289495
    },
    {
        "timestamp": 1527072000000,
        "open": 1.288975,
        "low": 1.288625,
        "close": 1.28899,
        "high": 1.28899
    },
    {
        "timestamp": 1527071700000,
        "open": 1.288825,
        "low": 1.288815,
        "close": 1.28897,
        "high": 1.289245
    },
    {
        "timestamp": 1527071400000,
        "open": 1.28924,
        "low": 1.288805,
        "close": 1.28882,
        "high": 1.28924
    },
    {
        "timestamp": 1527071100000,
        "open": 1.28872,
        "low": 1.288475,
        "close": 1.289245,
        "high": 1.289305
    },
    {
        "timestamp": 1527070800000,
        "open": 1.2884,
        "low": 1.288395,
        "close": 1.288715,
        "high": 1.289055
    },
    {
        "timestamp": 1527070500000,
        "open": 1.28837,
        "low": 1.288315,
        "close": 1.288395,
        "high": 1.28864
    },
    {
        "timestamp": 1527070200000,
        "open": 1.28834,
        "low": 1.28815,
        "close": 1.288365,
        "high": 1.28844
    },
    {
        "timestamp": 1527069900000,
        "open": 1.288385,
        "low": 1.28811,
        "close": 1.288325,
        "high": 1.288475
    },
    {
        "timestamp": 1527069600000,
        "open": 1.28789,
        "low": 1.287525,
        "close": 1.28839,
        "high": 1.28849
    },
    {
        "timestamp": 1527069300000,
        "open": 1.28848,
        "low": 1.287865,
        "close": 1.28788,
        "high": 1.28851
    },
    {
        "timestamp": 1527069000000,
        "open": 1.2887,
        "low": 1.28847,
        "close": 1.288475,
        "high": 1.288825
    },
    {
        "timestamp": 1527068700000,
        "open": 1.288965,
        "low": 1.288705,
        "close": 1.28871,
        "high": 1.289045
    },
    {
        "timestamp": 1527068400000,
        "open": 1.288865,
        "low": 1.28878,
        "close": 1.28896,
        "high": 1.289065
    },
    {
        "timestamp": 1527068100000,
        "open": 1.288285,
        "low": 1.28823,
        "close": 1.28887,
        "high": 1.28888
    },
    {
        "timestamp": 1527067800000,
        "open": 1.28806,
        "low": 1.287805,
        "close": 1.28828,
        "high": 1.2883
    },
    {
        "timestamp": 1527067500000,
        "open": 1.287465,
        "low": 1.287235,
        "close": 1.288055,
        "high": 1.28807
    },
    {
        "timestamp": 1527067200000,
        "open": 1.287465,
        "low": 1.287445,
        "close": 1.28746,
        "high": 1.2878
    },
    {
        "timestamp": 1527066900000,
        "open": 1.287395,
        "low": 1.28718,
        "close": 1.28746,
        "high": 1.2876
    },
    {
        "timestamp": 1527066600000,
        "open": 1.287395,
        "low": 1.28723,
        "close": 1.28739,
        "high": 1.287565
    },
    {
        "timestamp": 1527066300000,
        "open": 1.28755,
        "low": 1.28698,
        "close": 1.28739,
        "high": 1.28761
    },
    {
        "timestamp": 1527066000000,
        "open": 1.287575,
        "low": 1.287385,
        "close": 1.28756,
        "high": 1.28772
    },
    {
        "timestamp": 1527065700000,
        "open": 1.287855,
        "low": 1.287455,
        "close": 1.28758,
        "high": 1.2879
    },
    {
        "timestamp": 1527065400000,
        "open": 1.28731,
        "low": 1.28731,
        "close": 1.287865,
        "high": 1.28796
    },
    {
        "timestamp": 1527065100000,
        "open": 1.28771,
        "low": 1.28729,
        "close": 1.287305,
        "high": 1.28787
    },
    {
        "timestamp": 1527064800000,
        "open": 1.287715,
        "low": 1.287225,
        "close": 1.287715,
        "high": 1.287745
    },
    {
        "timestamp": 1527064500000,
        "open": 1.28805,
        "low": 1.287565,
        "close": 1.287725,
        "high": 1.288075
    },
    {
        "timestamp": 1527064200000,
        "open": 1.28812,
        "low": 1.287785,
        "close": 1.288055,
        "high": 1.28824
    },
    {
        "timestamp": 1527063900000,
        "open": 1.287795,
        "low": 1.287545,
        "close": 1.28811,
        "high": 1.288125
    },
    {
        "timestamp": 1527063600000,
        "open": 1.28889,
        "low": 1.287745,
        "close": 1.287775,
        "high": 1.28903
    },
    {
        "timestamp": 1527063300000,
        "open": 1.28828,
        "low": 1.28828,
        "close": 1.288895,
        "high": 1.28896
    },
    {
        "timestamp": 1527063000000,
        "open": 1.288775,
        "low": 1.288095,
        "close": 1.288255,
        "high": 1.288855
    },
    {
        "timestamp": 1527062700000,
        "open": 1.28854,
        "low": 1.2885,
        "close": 1.28878,
        "high": 1.289135
    },
    {
        "timestamp": 1527062400000,
        "open": 1.28823,
        "low": 1.288035,
        "close": 1.288545,
        "high": 1.28857
    },
    {
        "timestamp": 1527062100000,
        "open": 1.28793,
        "low": 1.28793,
        "close": 1.288225,
        "high": 1.28845
    },
    {
        "timestamp": 1527061800000,
        "open": 1.287985,
        "low": 1.28788,
        "close": 1.287935,
        "high": 1.288285
    },
    {
        "timestamp": 1527061500000,
        "open": 1.287765,
        "low": 1.287605,
        "close": 1.287975,
        "high": 1.288065
    },
    {
        "timestamp": 1527061200000,
        "open": 1.288385,
        "low": 1.28755,
        "close": 1.287775,
        "high": 1.288635
    },
    {
        "timestamp": 1527060900000,
        "open": 1.28854,
        "low": 1.288025,
        "close": 1.28838,
        "high": 1.288605
    },
    {
        "timestamp": 1527060600000,
        "open": 1.287245,
        "low": 1.287245,
        "close": 1.288545,
        "high": 1.288585
    },
    {
        "timestamp": 1527060300000,
        "open": 1.287405,
        "low": 1.28688,
        "close": 1.28724,
        "high": 1.287645
    },
    {
        "timestamp": 1527060000000,
        "open": 1.2873,
        "low": 1.286715,
        "close": 1.28738,
        "high": 1.2874
    }
]


module.exports = {
  findStrategy: {
    options: {
      populationCount: 100,
      generationCount: 300,
      selectionAmount: 10,
      leafValueMutationProbability: 0.5,
      leafSignMutationProbability: 0.3,
      logicalNodeMutationProbability: 0.3,
      leafIndicatorMutationProbability: 0.2,
      crossoverProbability: 0.03,
      indicators: [ 'CCI', 'MACD', 'RSI', 'SMA15_SMA50' ],
      strategy: { buy: {}, sell: {} }
    },
    candlesticks: globalCandleSticks
  }
};
